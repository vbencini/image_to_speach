# Image to speech

IMPORTANT: The project is a transcription of this video https://www.youtube.com/watch?v=_j7JEDWuqLE&t=438s

In this project we create an app that tell a story generated from an image. It combines the power of Huggingface models and LangChain. In particulare the following models were used:

- Image to text: Salesforce/blip-image-captioning-base (HuggingFace) -> FREE
- Story generation: gpt-3.5-turbo (OpanAI) (HuggingFace) -> NOT FREE
- Speach generation: espnet/kan-bayashi_ljspeech_vits -> FREE

## Useful infro before we start

In order to run the project you need an HuggingFace and an OpenAI token. 

The HuggingFace token can be generated for free. It will be sufficient to create an account. See instructions https://huggingface.co/docs/hub/security-tokens
 
For OpenAI you need to provide payment information, since the service is not for free. See instructions: https://platform.openai.com/docs/quickstart?context=python

## Installation

The application does not have a builder. You can simply clone the repository:

```bash
git clone https://gitlab.cern.ch/vbencini/image_to_speach.git 
```

Create and activate a virtual environment in Python:

```bash
 python -m venv /my_env
 source /my_env/bin/activate
```

or Anaconda

```bash
 conda create -n my_env python=3.9
 conda activate my_env
```

Then you can install the required packages executing the command:

```bash
 pip install -r requirements.txt
```
## Usage

To use the app simply run the following command:
```bash
 streamlit run app.py
```
