from dotenv import find_dotenv, load_dotenv
from transformers import pipeline
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain
from langchain.llms.huggingface_hub import HuggingFaceHub
import requests
import os
from langchain.llms import OpenAI
import streamlit as st


load_dotenv(find_dotenv())

OPENAI_API_TOKEN = os.getenv("OPENAI_API_TOKEN")
os.environ["OPENAI_API_KEY"] = OPENAI_API_TOKEN
HUGGINGFACEHUB_API_TOKEN = os.getenv("HUGGINGFACEHUB_API_TOKEN")

def img2text(url):
    image_to_text = pipeline("image-to-text", model="Salesforce/blip-image-captioning-base")

    text = image_to_text(url)[0]['generated_text']

    return text



scenario = img2text('test_image.jpg')

def generate_story(scenario):
    template = '''
    You are a story teller;
    You can generate a short story based on a simple narrative, the story should be no more than 20 words;

    CONTEXT: {scenario}
    STORY:
    '''

    prompt = PromptTemplate(template=template, input_variables=['scenario'])

    story_line = LLMChain(llm=OpenAI(temperature=1, model_name='gpt-3.5-turbo'), prompt=prompt, verbose=True)

    story = story_line.run(scenario=scenario)
    print(story)
    return story




def text2speach(message):

    API_URL = "https://api-inference.huggingface.co/models/espnet/kan-bayashi_ljspeech_vits"
    headers = {"Authorization": f"Bearer {HUGGINGFACEHUB_API_TOKEN}"}
    def query(payload):
        response = requests.post(API_URL, headers=headers, json=payload)
        return response
        
    response = query({
        "inputs": message,
    })
    # You can access the audio with IPython.display for example
    with open('audio.flac', 'wb') as file:
        file.write(response.content)

def main():
    st.set_page_config(page_title="Image to audio story", page_icon = "")

    st.header("Turn image into audio story")
    uploaded_file = st.file_uploader("Choose and image...", type="jpg")

    if uploaded_file is not None:
        print(uploaded_file)
        bytes_data = uploaded_file.getvalue()
        with open(uploaded_file.name, "wb") as file:
            file.write(bytes_data)
        st.image(uploaded_file, caption="Uploaded image", 
                 use_column_width=True)
        scenario = img2text(uploaded_file.name)
        story = generate_story(scenario)
        text2speach(story)

        with st.expander("scenario"):
            st.write(scenario)
        with st.expander("story"):
            st.write(story)

        st.audio("audio.flac")

if __name__ == '__main__':
    main()